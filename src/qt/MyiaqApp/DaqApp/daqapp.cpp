#include <QTime>

#include "daqapp.h"

DaqApp::DaqApp(QObject *parent) : QObject(parent)
{
    connect(&_sensors, &IDataFeed::stateChanged, this, &DaqApp::_onStateChanged);

    _cron.setInterval(2500 /*ms*/);
    connect(&_cron, &QTimer::timeout, this, &DaqApp::_onTimeout);
}


void DaqApp::_onTimeout()
{
    _sht21.readTemperature(_t);
    _sht21.readHumidity(_hr);
    _iaqcore.readCo2Tvoc(_co2, _tvoc);
    double als;
    _veml7700.readAmbientLight(als);

    _lux.addSample(als);

    QJsonObject T = {
        {"val", _t}
        , {"unit", "°C"}
    };

    QJsonObject HR = {
        {"val", _hr}
        , {"unit", "%"}
    };

    QJsonObject COV = {
        {"val", _tvoc}
        , {"unit", "ppb"}
    };

    QJsonObject CO2 = {
        {"val", _co2}
        , {"unit", "ppm"}
    };

    QJsonObject LUX = {
        {"val", _lux.isReady() ? _lux.getMedian() : 500}
        , {"unit", "lux"}
    };

    QJsonObject sensorsValue = {
        {"T"  , T}
        , {"HR", HR}
        , {"COV", COV}
        , {"CO2", CO2}
        , {"LUX", LUX}
    };

    _sensors.push(sensorsValue);
}

void DaqApp::_onStateChanged(IDataFeed::FeedState state) {
    switch(state) {
        case IDataFeed::Ready:
            qDebug() << "cron start";
            _cron.start();
            break;
        case IDataFeed::Error:
        case IDataFeed::Busy :
            _cron.stop();
            break;
    }
}
