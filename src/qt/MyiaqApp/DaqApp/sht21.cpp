#include <QThread>

#include "sht21.h"

Sht21::Sht21(QObject *parent) : QObject(parent)
  , _bcm(Bcm2835Wrapper::getInstance())
{

}

int Sht21::readTemperature(double& theTemp)
{
    uint8_t cmd[] = {0xf3};
    uint8_t resp[ 3 ];
    uint16_t rawTemperature;

    _bcm.i2c_write(_SHT21_ADDR, cmd, 1);

    QThread::msleep(85);

    _bcm.i2c_read(_SHT21_ADDR, resp, 3);

    rawTemperature = (resp[0 ] << 8) | (resp[ 1 ] & 0xfc);

    theTemp = 172.72 * (rawTemperature / 65536.0) - 46.85;

    return 0;
}


int Sht21::readHumidity(double& theHum)
{
    uint8_t cmd[] = {0xf5};
    uint8_t resp[ 3 ];
    uint16_t rawHumidity;

    _bcm.i2c_write(_SHT21_ADDR, cmd, 1);

    QThread::msleep(50); // > 29ms selon datasheet

    _bcm.i2c_read(_SHT21_ADDR, resp, 3);

    rawHumidity = (resp[0 ] << 8) | (resp[ 1 ] & 0xf0);

    theHum = 125.0 * (rawHumidity / 65536.0) - 6.0;

    return 0;

}
