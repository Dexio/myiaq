#ifndef DAQAPP_H
#define DAQAPP_H

#include <QObject>
#include <QTimer>

#include "sensorsdatafeed.h"
#include "sht21.h"
#include "iaqcore.h"
#include "veml7700.h"
#include "noisefilter.h"

class DaqApp : public QObject
{
    Q_OBJECT
public:
    explicit DaqApp(QObject *parent = nullptr);

private:
    SensorsDataFeed _sensors;
    Sht21 _sht21;
    IaqCore _iaqcore;
    Veml7700 _veml7700;

    QTimer _cron;

    uint16_t _co2;
    uint16_t _tvoc;
    double _t;
    double _hr;
    NoiseFilter<double, 5> _lux;

private slots:
    void _onTimeout();
    void _onStateChanged(IDataFeed::FeedState state);
};

#endif // DAQAPP_H
