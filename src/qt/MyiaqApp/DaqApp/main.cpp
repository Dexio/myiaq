#include <QCoreApplication>

#include "daqapp.h"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    DaqApp Acquisition;

    return a.exec();
}
