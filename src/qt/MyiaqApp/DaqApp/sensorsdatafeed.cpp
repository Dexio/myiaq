#include <QJsonDocument>

#include "sensorsdatafeed.h"

SensorsDataFeed::SensorsDataFeed()
    : _mqttClient(nullptr), _subscription(nullptr)
{
    _mqttClient = new QMqttClient();

    connect(_mqttClient, &QMqttClient::connected, this, &SensorsDataFeed::_onMqttConnected);

    _mqttClient->setHostname(_BROKER_URL);
    _mqttClient->setPort(_BROKER_PORT);
    _mqttClient->connectToHost();
}

void SensorsDataFeed::push(QJsonObject value)
{
    QJsonDocument jsonDoc(value);
    const QMqttTopicName topicName = QMqttTopicName(_SENSORS_TOPIC);

    QByteArray topicValue = jsonDoc.toJson(QJsonDocument::Indented);
    _mqttClient->publish(topicName, topicValue);
}

QJsonObject SensorsDataFeed::pull()
{
    return _sensorsValue;
}

void SensorsDataFeed::_onMqttConnected()
{
    emit stateChanged(Ready);
}

void SensorsDataFeed::_onMqttMsgReceived(const QByteArray &message)
{
    QJsonParseError jsonErr;
    QJsonDocument jsonDoc = QJsonDocument::fromJson(message, &jsonErr);

    qDebug() << "jsonErr : " << jsonErr.errorString();

    if( jsonDoc.isObject()) {
        _sensorsValue = jsonDoc.object();
        qDebug() << "SUCCESS : new JSON object read from topic value";
    } else {
         qDebug() << "ERROR : topic value is not a JSON object !";
    }

    emit dataReady();
}


