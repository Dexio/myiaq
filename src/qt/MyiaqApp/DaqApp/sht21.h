#ifndef SHT21_H
#define SHT21_H

#include <QObject>

#include "bcm2835wrapper.h"

class Sht21 : public QObject
{
    Q_OBJECT
public:
    explicit Sht21(QObject *parent = nullptr);
    int readTemperature(double& theTemp);
    int readHumidity(double& theHum);

private:
    Bcm2835Wrapper& _bcm;
    const uint8_t _SHT21_ADDR = 0x40;

signals:

};

#endif // SHT21_H
