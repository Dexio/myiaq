#ifndef VEML7700_H
#define VEML7700_H

#include <QObject>

#include "bcm2835wrapper.h"


class Veml7700 : public QObject
{
    Q_OBJECT
public:
    explicit Veml7700(QObject *parent = nullptr);
    int readAmbientLight(double& theLight);

public slots:

private:
    Bcm2835Wrapper& _bcm;
    const uint8_t _VEML7700_ADDR = 0x10;

signals:

};

#endif // VEML7700_H
