#include <QDebug>

#include "veml7700.h"


Veml7700::Veml7700(QObject *parent) : QObject(parent)
  , _bcm(Bcm2835Wrapper::getInstance())
{
    uint8_t cmd[ 3 ] = {
        0x00    // Command code 0
        , 0x10  // -+
        , 0xc0  // -+-> Gain 1/8 IT 800ms Persistance 1 Int disable Power On
    };

    _bcm.i2c_write(_VEML7700_ADDR, cmd, 3); //
}

int Veml7700::readAmbientLight(double& theLight)
{
    uint8_t cmd[ 3 ] = {
        0x04    // Command code
    };
    uint8_t buf[ 2 ];
    int ret = -1;

    Bcm2835Wrapper::i2c_err_t  status = _bcm.i2c_write_read_rs(_VEML7700_ADDR, cmd, 1, buf, 2);
    if( status == Bcm2835Wrapper::SUCCESS) {
        theLight = 0.0576*((buf[ 1 ] << 8) | buf[ 0 ]);
        ret = 0;
    } else {
        qDebug() << "erreur lecture i2c sur VEML7700 : " << status ;
        theLight = 0.0;
    }
    return ret;
}

