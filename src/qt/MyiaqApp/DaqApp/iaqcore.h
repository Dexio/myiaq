#ifndef IAQCORE_H
#define IAQCORE_H

#include <QObject>

#include "bcm2835wrapper.h"


class IaqCore : public QObject
{
    Q_OBJECT
public:
    explicit IaqCore(QObject *parent = nullptr);
    int readCo2Tvoc(uint16_t& theCo2, uint16_t& theTvoc);

public slots:

private:
    Bcm2835Wrapper& _bcm;
    const uint8_t _IAQ_CORE_ADDR = 0x5A;

signals:

};

#endif // IAQCORE_H
