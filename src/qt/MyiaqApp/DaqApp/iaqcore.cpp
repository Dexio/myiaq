#include <QDebug>

#include "iaqcore.h"


IaqCore::IaqCore(QObject *parent) : QObject(parent)
  , _bcm(Bcm2835Wrapper::getInstance())
{
}

int IaqCore::readCo2Tvoc(uint16_t& theCo2, uint16_t& theTvoc)
{
    uint8_t resp[ 9 ];
    int ret = -1;
    Bcm2835Wrapper::i2c_err_t  status = _bcm.i2c_read(_IAQ_CORE_ADDR, resp, 9);
    //bcm2835_i2c_setSlaveAddress(IAQ_CORE_ADDR); //Sélectionne le capteur
    if( status == Bcm2835Wrapper::SUCCESS) {
        theCo2 = (resp[ 0 ] << 8) | resp[ 1 ];
        theTvoc = (resp[ 7 ] << 8) | resp[ 8 ];
        ret = 0;
    } else {
        qDebug() << "erreur lecture i2c : " << status ;
        theCo2 = theTvoc = 0xffff;
    }
    return ret;
}

