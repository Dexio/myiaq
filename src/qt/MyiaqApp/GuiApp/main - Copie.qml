import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12

import "qrc:/"

import tech.lyceebenoit.myiaq 1.0

Window {
   visible: true
   width: 800
   height: 480



   Rectangle
   {
      id: background
       anchors.fill: parent
       color: "black"
   }

   ColumnLayout {
      Layout.fillWidth: true
      Layout.fillHeight: true
      anchors.fill: parent

      Item {
         //DebugRectangle {}
         id: gridBox
         width: 800
         height: 320
         Layout.alignment: Qt.AlignHCenter | Qt.AlignTop

         GridLayout {
            columns:  4
            rows: 2
            anchors.fill: parent

            Image {
               //DebugRectangle {}
               Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
               sourceSize.height: 50
               source: "temp_icon.svg"
            }

            Image {
               //DebugRectangle {}
               Layout.alignment: Qt.AlignHCenter
               sourceSize.height: 50
               source: "humidity_icon.svg"
            }

            Image {
               //DebugRectangle {}
               Layout.alignment: Qt.AlignHCenter
               sourceSize.height: 50
               source: "co2_icon.svg"
            }

            Image {
               //DebugRectangle {}
               Layout.alignment: Qt.AlignHCenter
               sourceSize.height: 50
               source: "tvoc_icon.svg"
            }

            Text {
               //DebugRectangle {}
               Layout.alignment: Qt.AlignHCenter
               text: "Température"
               color : "#299DD2"
               font.bold: true
            }

            Text {
               //DebugRectangle {}
               Layout.alignment: Qt.AlignHCenter
               text: "Humidité"
               color : "#299DD2"
               font.bold: true
            }

            Text {
               //DebugRectangle {}
               Layout.alignment: Qt.AlignHCenter
               textFormat: Text.RichText
               text: "CO<sub>2</sub>"
               color : "#299DD2"
               font.bold: true
            }

            Text {
               //DebugRectangle {}
               Layout.alignment: Qt.AlignHCenter
               text: "COVs"
               color : "#299DD2"
               font.bold: true
            }

            Knob {
               //DebugRectangle {}
               id: knobT
               objectName : "knobT"
               x: 0
               y: 83
               width: 195
               height: 195
               Layout.alignment: Qt.AlignHCenter
               from:0
               to: 50
               reverse: false
               stopLow: 15
               stopMid: 25
               stopHigh: 40
            }

            Knob {
               //DebugRectangle {}
               id: knobHR
               objectName : "knobHR"
               x: 0
               y: 83
               width: 195
               height: 195
               Layout.alignment: Qt.AlignHCenter
               from:0
               to: 100
               reverse: false
               stopLow: 40
               stopMid: 60
               stopHigh: 80
             }

            Knob {
                 //DebugRectangle {}
                 id: knobCO2
                 x: 0
                 y: 83
                 width: 195
                 height: 195
                 Layout.alignment: Qt.AlignHCenter
                 from:0
                 to: 1000
                 reverse: false
               }

            Knob {
               //DebugRectangle {}
               id: knobCOV
               x: 0
               y: 83
               width: 195
               height: 195
               Layout.alignment: Qt.AlignHCenter
               from:0
               to: 1000
               reverse: false
            }

         } // GridLayout

      } // Item

      Item {
         width: 800
         height: 160-6
         Layout.alignment: Qt.AlignHCenter | Qt.AlignTop

         Row {
             id: row
            anchors.fill: parent
            spacing: 5

            // Bouton OFF
            DelayButton {
                id: control
                anchors.bottom: parent.bottom
                checked: false
                delay: 5000

                onActivated: {
                    console.debug("Activated!")
                    checked: false
                    Qt.callLater(Qt.quit)
                }

                background: Rectangle {
                    implicitWidth: 32
                    implicitHeight: 32
                    opacity: enabled ? 1 : 0.3
                    color: control.down ? "#17a81a" : "#21be2b"
                    radius: size / 2

                    readonly property real size: Math.min(control.width, control.height)
                    width: size
                    height: size
                    anchors.centerIn: parent

                    Image {
                        id: icon
                        width: 32
                        height: 32
                        anchors.horizontalCenter: parent.horizontalCenter
                        anchors.verticalCenter: parent.verticalCenter
                        source: "On-Off-Switch.svg"
                    }

                    Canvas {
                        id: canvas
                        anchors.fill: parent

                        Connections {
                            target: control
                            onProgressChanged: canvas.requestPaint()
                        }

                        onPaint: {
                            var ctx = getContext("2d")
                            ctx.clearRect(0, 0, width, height)
                            ctx.strokeStyle = "white"
                            ctx.lineWidth = parent.size / 20
                            ctx.beginPath()
                            var startAngle = Math.PI / 5 * 3
                            var endAngle = startAngle + control.progress * Math.PI / 5 * 9
                            ctx.arc(width / 2, height / 2, width / 2 - ctx.lineWidth / 2 - 2, startAngle, endAngle)
                            ctx.stroke()
                        }
                    }
                }
            }

            // Zone de notification
            GroupBox {
               id: idNotification
               title: "Notification"
               //property alias notificationOpacity: opacity
               width: 480
               height: 160
               anchors.verticalCenter: parent.verticalCenter
               opacity: 1.0

               label: Label {
                   x: parent.leftPadding
                   width: parent.availableWidth
                   text: parent.title
                   color: "#299DD2"
                   elide: Text.ElideRight
               }

               Column {
                  Text {
                     DebugRectangle{}
                     id: co2Alert
                     text: "alerte CO2"
                     color : "#299DD2"
                  }
                  Text {
                     //DebugRectangle{}
                     id: covAlert
                     text: "alerte COV"
                     color : "#299DD2"
                  }
               } // Column
            } // Groupbox

            // Zone date & heure
            Column {
               anchors.verticalCenter: parent.verticalCenter

               Text {
                  //DebugRectangle{}
                  id: idTime
                  //text: Qt.formatDateTime(new Date(), "ddd d MMMM")
                  color : "#299DD2"
                  font.pointSize: 40
               }

               Text {
                  id: idDate
                  //DebugRectangle{}
                  //text: Qt.formatDateTime(new Date(), "hh:mm")
                  color : "#299DD2"
                  font.pointSize: 18
               }

               Timer {
                  interval: 500
                  running: true
                  repeat: true

                  onTriggered: {
                     var date = new Date()
                     idTime.text = date.toLocaleTimeString(Qt.locale("fr_FR"), "hh:mm:ss")
                     idDate.text = date.toLocaleDateString(Qt.locale("fr_FR"), "ddd d MMMM")
                     //idDate.text = Qt.formatDateTime(date, "ddd d MMMM")
                     //idTime.text = Qt.formatDateTime(date, "hh:mm:ss AP")
                  }
               }

            } // Column

            // Bouton "Paramètres"
            Button {
               icon: "config_icon.svg"
               anchors.bottom: parent.bottom
            }
      } // RowLayout
      } // Item

   } // ColumnLayout

    SensorsDataFeed {
        id: mySensors
        onDataReady: {
            console.log("Data Feed Ready");
            var jsonObject = mySensors.pull();
            console.log("T : " + jsonObject.T.val);
            knobT.value = jsonObject.T.val;
            knobHR.value = jsonObject.HR.val;
            knobCO2.value = jsonObject.CO2.val;
            knobCOV.value = jsonObject.COV.val;
        }
    }
} // Item
