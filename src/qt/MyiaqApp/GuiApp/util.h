#ifndef UTIL_H
#define UTIL_H

#include <QObject>
/**
 * Classe utilitaire dont les méthodes sont appelables depuis QML sans instancier l'objet
 * (donc l'équivalent de méthodes statiques)
 * Voir https://evileg.com/en/post/263/[QML - Tutorial 029. Registering a Singleton object to use "Static" methods in QML]
 * pour les détails d'implémentation
 */
#include <QQmlEngine>
#include <QJSEngine>

class Util : public QObject
{
    Q_OBJECT
public:
    explicit Util(QObject *parent = nullptr);
    Q_INVOKABLE  QString getIpAddress(QString interface);
signals:

};

static QObject *utilProvider(QQmlEngine *engine, QJSEngine *scriptEngine)
{
    Q_UNUSED(engine)
    Q_UNUSED(scriptEngine)

    Util *singletonClass = new Util();
    return singletonClass;
}
#endif // UTIL_H
