#include <stdio.h>
#include <qdebug.h>

#include "util.h"


Util::Util(QObject *parent) : QObject(parent)
{

}

QString Util::getIpAddress(QString interface)
{
    FILE * fp;
    char buf[ 512 ];
    QString cmd =  "ip -4 -br addr show " + interface + " | tr -s [:blank:] | cut -d' ' -f3";
    QString ipAddr;
    fp = popen(cmd.toLocal8Bit().data(), "r");
    while(fgets(buf, sizeof (buf), fp) != nullptr) {
        ipAddr += buf;
    }

    if(ipAddr.isEmpty()) {
        ipAddr = "n/a";
    }
    //qDebug() << "ip addr " << interface << " : " << ipAddr;

    return ipAddr;
}
