import QtQuick 2.0

//all the automatic meagurements are made from height of Item
Item {
    id: knob
    transformOrigin: Item.Center

    property var metaData : ["from", "to", "value",
        "reverse",
        "fromAngle", "toAngle",
        "lineWidth", "fontSize",
        "knobBackgroundColor", "knobColor",
        "title", "titleFont", "titleFontSize", "titleFontColor"]

    //value parameters
    property int from:0
    property int value: 0
    property int to: 1500

    property int stopLow: 0
    property int stopMid: 0
    property int stopHigh: 0

//progress from right to left
    property bool reverse: false

    //progress circle angle
    property double fromAngle: - Math.PI/2 + (10.0/180)*Math.PI
    property double toAngle: 3 *Math.PI / 2 - (10.0/180.0)*Math.PI

    property int lineWidth: height / 20
    property int fontSize: height / 8

    property color knobBackgroundColor: Qt.rgba(0.90, 0.84, 0.91) //Qt.rgba(0.1, 0.1, 0.1, 0.1)
    property color knobColor: Qt.rgba(0, 0, 1, 1)

    property string title: ""
    property alias titleFont: labelTitle.font.family
    property alias titleFontSize: labelTitle.font.pointSize
    property alias titleFontColor: labelTitle.color

    onValueChanged: {
        knob.value = value

        if((value > 0) && (value < knob.stopLow)) {
            knob.knobColor = "blue"
        }
        if((value > knob.stopLow) && (value < knob.stopMid)) {
            knob.knobColor = "green"
        }
        if((value > knob.stopMid) && (value < knob.stopHigh)) {
            knob.knobColor = "orange"
        }
        if( value > knob.stopHigh) {
            knob.knobColor = "red"
        }
        canvas.requestPaint()
        background.requestPaint()
        label.text = value //value.toFixed(2);
    }

    function update(value) {
        knob.value = value
        canvas.requestPaint()
        background.requestPaint()
        label.text = value.toFixed(2);
    }

    Text {
        id: labelTitle
        y: 0
        text: knob.title
        color: Qt.rgba(0, 0, 0., 0.5)
        horizontalAlignment: Text.AlignHCenter
        anchors.horizontalCenter: parent.horizontalCenter
    }

    Canvas {
        id: background
        width: parent.height
        height: parent.height
        antialiasing: true

        property int radius: background.width/2

        onPaint: {

            if (knob.title !== "") {
                background.y = labelTitle.height
                background.x = labelTitle.height/2
                background.height = parent.height - labelTitle.height/2
                background.width = parent.height - labelTitle.height/2
                background.radius = background.height /2
            }

            var centreX = background.width / 2.0;
            var centreY = background.height / 2.0;

            var ctx = background.getContext('2d');
            ctx.strokeStyle = knob.knobBackgroundColor;
            ctx.lineWidth = knob.lineWidth;
            ctx.lineCap = "round"
            ctx.beginPath();
            ctx.clearRect(0, 0, background.width, background.height);
            ctx.arc(centreX, centreY, radius - knob.lineWidth, knob.fromAngle, knob.toAngle, false);
            ctx.stroke();
        }
    }

    Canvas {
        id:canvas
        width: parent.height
        height: parent.height
        antialiasing: true

        property double step: knob.value / (knob.to - knob.from) * (knob.toAngle - knob.fromAngle)
        property int radius: height/2

        Text {
            id: label
            width: 40
            height: 12
            color: "#299DD2"
            font.bold: true
            z: 1
            font.pointSize: knob.fontSize
            text: knob.value.toString()
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            anchors.fill: parent
        }

        onPaint: {

            if (knob.title !== "") {
                canvas.y = labelTitle.height
                canvas.x = labelTitle.height/2
                canvas.height = parent.height - labelTitle.height/2
                canvas.width = parent.height - labelTitle.height/2
                canvas.radius = canvas.height /2
            }

            var centreX = canvas.width / 2.0;
            var centreY = canvas.height / 2.0;

            var ctx = canvas.getContext('2d');
            ctx.strokeStyle = knob.knobColor;
            ctx.lineWidth = knob.lineWidth;
            ctx.lineCap = "round"
            ctx.beginPath();
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            if (knob.reverse) {
                ctx.arc(centreX, centreY, radius - knob.lineWidth, knob.toAngle, knob.toAngle - step, true);
            } else {
                ctx.arc(centreX, centreY, radius - knob.lineWidth, knob.fromAngle, knob.fromAngle + step, false);
            }
            ctx.stroke();
        }
    }
}
