import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12
import QtQuick.Dialogs 1.2

import "qrc:/"

import tech.lyceebenoit.myiaq 1.0

Window {
   visible: true
   width: 800
   height: 480

   Rectangle
   {
      id: background
       anchors.fill: parent
       color: "whitesmoke"
   }

   ColumnLayout {
       spacing: 0

      // Zone supérieure affichage (valeurs de capteur)
      Item {
         //DebugRectangle {}
         id: gridBox
         Layout.preferredWidth: 800
         Layout.preferredHeight: 320
         Layout.alignment: Qt.AlignHCenter | Qt.AlignTop

         GridLayout {
            columns:  4
            rows: 3

            Image {
               //DebugRectangle {}
               Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
               sourceSize.height: 50
               source: "temp_icon.svg"
            }

            Image {
               //DebugRectangle {}
               Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
               sourceSize.height: 50
               source: "humidity_icon.svg"
            }

            Image {
               //DebugRectangle {}
               Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
               sourceSize.height: 50
               source: "co2_icon.svg"
            }

            Image {
               //DebugRectangle {}
               Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
               sourceSize.height: 50
               source: "tvoc_icon.svg"
            }

            Text {
               //DebugRectangle {}
               Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
               text: "Température"
               color : "#299DD2"
               font.bold: true
            }

            Text {
               //DebugRectangle {}
               Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
               text: "Humidité"
               color : "#299DD2"
               font.bold: true
            }

            Text {
               //DebugRectangle {}
               Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
               textFormat: Text.RichText
               text: "CO<sub>2</sub>"
               color : "#299DD2"
               font.bold: true
            }

            Text {
               //DebugRectangle {}
               Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
               text: "COVs"
               color : "#299DD2"
               font.bold: true
            }

            Knob {
               //DebugRectangle {}
               id: knobT
               objectName : "knobT"
               x: 0
               y: 83
               Layout.preferredWidth: 195
               Layout.preferredHeight: 195
               Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
               from:0
               to: 50
               reverse: false
               stopLow: 15
               stopMid: 25
               stopHigh: 40
            }

            Knob {
               //DebugRectangle {}
               id: knobHR
               objectName : "knobHR"
               x: 0
               y: 83
               Layout.preferredWidth: 195
               Layout.preferredHeight: 195
               Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
               from:0
               to: 100
               reverse: false
               stopLow: 40
               stopMid: 60
               stopHigh: 80
             }

            Knob {
                 //DebugRectangle {}
                 id: knobCO2
                 x: 0
                 y: 83
                 Layout.preferredWidth: 195
                 Layout.preferredHeight: 195
                 Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                 from:0
                 to: 2000
                 reverse: false
                 stopLow: 300
                 stopMid: 1000
                 stopHigh: 1500
               }

            Knob {
               //DebugRectangle {}
               id: knobCOV
               x: 0
               y: 83
               Layout.preferredWidth: 195
               Layout.preferredHeight: 195
               Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
               from:0
               to: 600
               reverse: false
               stopLow: 75
               stopMid: 400
               stopHigh: 500
            }

         } // GridLayout

      } // Item

      // Zone inférieure affichage (bouton OFF, Notification, Date, bouton paramètres)
      Item {
         Layout.preferredWidth: 800
         Layout.preferredHeight: 160


         //DebugRectangle {}

         RowLayout {
            //anchors.fill: parent

            spacing: 5

            // Bouton OFF
            DelayButton {
               id: control
               //DebugRectangle {}
               //anchors.bottom: parent.bottom
               Layout.alignment: Qt.AlignHCenter | Qt.AlignBottom
               Layout.preferredWidth: 32
               Layout.preferredHeight: 32
               checked: false
               delay: 5000

               onActivated: {
                  console.debug("Activated!")
                  checked: false
                  Qt.callLater(Qt.quit)
               }

               background: Rectangle {
                  implicitWidth: 32
                  implicitHeight: 32
                  opacity: enabled ? 1 : 0.3
                  color: control.down ? "#17a81a" : "#21be2b"
                  radius: size / 2

                  readonly property real size: Math.min(control.width, control.height)
                  width: size
                  height: size
                  anchors.centerIn: parent

                              Image {
                  id: icon
                           width: 30
                     height: 30
                           anchors.horizontalCenter: parent.horizontalCenter
                           anchors.verticalCenter: parent.verticalCenter
                           source: "On-Off-Switch.svg"
                              }

                  Canvas {
                        id: canvas
                        anchors.fill: parent

                        Connections {
                           target: control
                           onProgressChanged: canvas.requestPaint()
                        }

                        onPaint: {
                           var ctx = getContext("2d")
                           ctx.clearRect(0, 0, width, height)
                           ctx.strokeStyle = "white"
                           ctx.lineWidth = parent.size / 20
                           ctx.beginPath()
                           var startAngle = Math.PI / 5 * 3
                           var endAngle = startAngle + control.progress * Math.PI / 5 * 9
                           ctx.arc(width / 2, height / 2, width / 2 - ctx.lineWidth / 2 - 2, startAngle, endAngle)
                           ctx.stroke()
                        }
                  }
               }
            }

            // Zone notification
            GroupBox {
               id: idNotification
               title: "Notification"
               //DebugRectangle {}

               //property alias notificationOpacity: opacity
               Layout.preferredWidth: 400
               Layout.preferredHeight: 160
               //anchors.verticalCenter: parent.verticalCenter
               opacity: 0.0
               //visible: false

               label: Label {
                   x: parent.leftPadding
                   width: parent.availableWidth
                   text: parent.title
                   color: "#299DD2"
                   elide: Text.ElideRight
               }

               Column {
                  Text {
                     //DebugRectangle{}
                     id: co2Alert
                     text: "alerte CO2"
                     color : "#299DD2"
                  }
                  Text {
                     //DebugRectangle{}
                     id: covAlert
                     text: "alerte COV"
                     color : "#299DD2"
                  }
               } // Column
            } // Groupbox

            // Zone date
            Column {
                id : today
                Layout.preferredWidth: 320
                //Layout.fillWidth: true
                Layout.fillHeight: true
                Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
               //DebugRectangle {}

               Text {
                  id: idTime
                  //DebugRectangle{}
                  //text: Qt.formatDateTime(new Date(), "ddd d MMMM")
                  color : "#299DD2"
                  font.pointSize: 40
               }

               Text {
                  id: idDate
                  //DebugRectangle{}
                  //text: Qt.formatDateTime(new Date(), "hh:mm")
                  color : "#299DD2"
                  font.pointSize: 18
               }

               Timer {
                  interval: 500
                  running: true
                  repeat: true

                  onTriggered: {
                     //var date = new Date('1995-09-30T03:24:00')
                     var date = new Date()
                     idTime.text = date.toLocaleTimeString(Qt.locale("fr_FR"), "hh:mm:ss")
                     idDate.text = date.toLocaleDateString(Qt.locale("fr_FR"), "ddd d MMMM")
                     //idDate.text = Qt.formatDateTime(date, "ddd d MMMM")
                     //idTime.text = Qt.formatDateTime(date, "hh:mm:ss AP")
                  }
               }

            } // Column

            // Bouton info
            Button {
                id: btnInfo
                //DebugRectangle {}
                onClicked: cfgInfoBox.open()
                //anchors.right: parent.right
                //anchors.bottom: parent.bottom
                Layout.alignment: Qt.AlignRight | Qt.AlignBottom
                Layout.preferredWidth: 32
                Layout.preferredHeight: 32

                background: Rectangle {
                    z: 1
                   implicitWidth: 32
                   implicitHeight: 32
                   opacity: enabled ? 1 : 0.3
                   color: "transparent"
                   radius: size / 2

                   readonly property real size: Math.min(btnInfo.width, btnInfo.height)
                   width: size
                   height: size

                    Image {
                        id: cfgIcon
                        width: 32
                        height: 32
                        anchors.centerIn: parent
                        source: "config_icon.svg"
                     }
                }
            }


         } // Row
      } // Item

   } // ColumnLayout

    SensorsDataFeed {
        id: mySensors
        onDataReady: {
            console.log("Data Feed Ready");
            var jsonObject = mySensors.pull();
            console.log("T : " + jsonObject.T.val);
            knobT.value = jsonObject.T.val;
            knobT.unitText = jsonObject.T.unit;
            knobHR.value = jsonObject.HR.val;
            knobHR.unitText = jsonObject.HR.unit;
            knobCO2.value = jsonObject.CO2.val;
            knobCO2.unitText = jsonObject.CO2.unit;
            knobCOV.value = jsonObject.COV.val;
            knobCOV.unitText = jsonObject.COV.unit;
            background.color = jsonObject.LUX.val < 100 ? 'black' : 'white';
        }
    }

    Popup {
        id: cfgInfoBox
        x:10
        y:10
        width: 600
        height: 350
        modal: true
        focus: true
        closePolicy: Popup.CloseOnEscape | Popup.CloseOnPressOutside
        anchors.centerIn: parent

        onClosed : cfgInfoBoxView.setCurrentIndex(0)

        background: Rectangle {
            //anchors.fill: popup
            border.color: "transparent"
            color: "transparent"
        }

        Rectangle {
            id: cfgInfoBoxFrame
            anchors.fill: parent
            radius: 50
            color: "whitesmoke"



            SwipeView {
                id: cfgInfoBoxView

                currentIndex: 0
                anchors.fill: parent
                clip: true // clipping du contenu durant la transition

//                Item {
//                    id: tresholds
//                    ListModel {
////                         id: thresholdsModel
////                         ListElement {type: 'T°'; low: knobT.stopLow; mid: knobT.stopMid; high: knobT.stopHigh}
////                         ListElement {type: 'HR'; low: knobHR.stopLow; mid: knobHR.stopMid; high: knobHR.stopHigh}
////                         ListElement {type: 'CO2'; low: knobCO2.stopLow; mid: knobCO2.stopMid; high: knobCO2.stopHigh}
////                         ListElement {type: 'COV'; low: knobCOV.stopLow; mid: knobCOV.stopMid; high: knobCOV.stopHigh}
////                         ListElement {type: 'T°'; low: 10; mid: 20; high: 30}
////                         ListElement {type: 'HR'; low: 100; mid: 200; high: 300}
////                         ListElement {type: 'CO2'; low: 1000; mid: 2000; high: 3000}
////                         ListElement {type: 'COV'; low: 10000; mid: 20000; high: 30000}
//                     }
//                    Component {
//                        id: thresholdsDelegate
//                        Column {
//                            id: tresholds
//                            Text{ text: type + ' Low : '+ low; anchors.horizontalCenter: parent.horizontalCenter}
//                            Text{ text: type + ' Mid : '+ mid; anchors.horizontalCenter: parent.horizontalCenter}
//                            Text{ text: type + ' High : '+ high; anchors.horizontalCenter: parent.horizontalCenter}
//                        }
//                    }
//                    TableView {
////                        anchors.horizontalCenter: view.horizontalCenter
////                        anchors.verticalCenter: view.verticalCenter
////                        anchors.fill: view
//                        //implicitWidth: 100
//                        implicitHeight: 350
//                          //model: thresholdsModel
//                        model: [{type: 'T°', low: knobT.stopLow, mid: knobT.stopMid, high: knobT.stopHigh}
//                            , {type: 'HR', low: knobHR.stopLow, mid: knobHR.stopMid, high: knobHR.stopHigh}
//                            , {type: 'CO2', low: knobCO2.stopLow, mid: knobCO2.stopMid, high: knobCO2.stopHigh}
//                            , {type: 'COV', low: knobCOV.stopLow, mid: knobCOV.stopMid, high: knobCOV.stopHigh}
//                            ]
//                          delegate: thresholdsDelegate
//                      }
//                }
                Item {
                    id: cfgNetwork

                    Text {
                        anchors.top: parent.top
                        anchors.horizontalCenter: parent.horizontalCenter
                        text: "Réseau"
                        color : "#299DD2"
                        font.italic: true
                        font.bold: true
                        font.underline: true
                        font.pixelSize: 26
                    }

                    Text{
                        anchors.centerIn: parent
                        anchors.fill: cfgInfoBoxView
                        text: "eth0\t: " + Util.getIpAddress("eth0") + "\nwlan0\t: " + Util.getIpAddress("wlan0")
                        color : "#299DD2"
                        font.bold: true

                    }
                }
                Item {

                    id: thirdPage
                    Text {
                        anchors.top: parent.top
                        anchors.horizontalCenter: parent.horizontalCenter
                        text: "Seuils"
                        color : "#299DD2"
                        font.italic: true
                        font.bold: true
                        font.underline: true
                        font.pixelSize: 26
                    }

                }
            }

            PageIndicator {
                id: indicator

                count: cfgInfoBoxView.count
                currentIndex: cfgInfoBoxView.currentIndex

                anchors.bottom: cfgInfoBoxView.bottom
                anchors.horizontalCenter: parent.horizontalCenter
            }
        }

        }



} // Item
