#ifndef SENSORSDATAFEED_H
#define SENSORSDATAFEED_H

#include <QMqttClient>
#include <QMqttTopicFilter>

#include <idatafeed.h>

class SensorsDataFeed : public IDataFeed
{
public:
    SensorsDataFeed();
    void push(QJsonObject value) override;
    QJsonObject pull() override;

private:
    QMqttClient * _mqttClient;
    QMqttSubscription * _subscription;
    const QString _BROKER_URL = "localhost";
//    const QString _BROKER_URL = "broker.hivemq.com";
//    const QString _BROKER_URL = "192.168.5.124";
//    const QString _BROKER_URL = "rpi-defrance.local";
    const int _BROKER_PORT = 1883;
    const QString _SENSORS_TOPIC = "MyIAQ/sensors";

    QJsonObject _sensorsValue;
    /* Format attendu :
{
   "T":{
      "val":21.3,
      "unit":"C"
   },
   "HR":{
      "val":40.5,
      "unit":"%"
   },
   "COV":{
      "val":8000,
      "unit":"ppb"
   },
   "CO2":{
      "val":870,
      "unit":"ppm"
   },
   "LUX":{
      "val": 150
      "unit":"lux"
   }
}
     */

private slots:
    void onMqttConnected();
    void onMqttMsgReceived(const QByteArray &message);

};

#endif // SENSORSDATAFEED_H
